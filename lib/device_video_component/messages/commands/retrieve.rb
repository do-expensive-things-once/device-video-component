module DeviceVideoComponent
  module Messages
    module Commands
      class Retrieve
        include Messaging::Message

        attribute :device_video_id, String
        attribute :device_id, String
        attribute :start_minute, String
      end
    end
  end
end
