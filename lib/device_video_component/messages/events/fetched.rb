module DeviceVideoComponent
  module Messages
    module Events
      class Fetched
        include Messaging::Message

        attribute :device_video_id, String
        attribute :device_id, String
        attribute :start_minute, String
        attribute :uri, String
      end
    end
  end
end
