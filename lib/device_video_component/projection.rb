module DeviceVideoComponent
  class Projection
    include EntityProjection
    include Messages::Events

    entity_name :device_video

    apply Fetched do |fetched|
      SetAttributes.(device_video, fetched, copy: [
        { :device_video_id => :id },
        :device_id,
        :start_minute
      ])

      device_video.uri = fetched.uri
    end

    apply Retrieved do |retrieved|
      device_video.id = retrieved.device_video_id

      device_video.retrieved(retrieved.workflow)
    end
  end
end
