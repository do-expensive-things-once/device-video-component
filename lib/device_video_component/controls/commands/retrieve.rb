module DeviceVideoComponent
  module Controls
    module Commands
      module Retrieve
        def self.example
          retrieve = Messages::Commands::Retrieve.new

          retrieve.device_id = DeviceVideo.device_id
          retrieve.start_minute = DeviceVideo.start_minute
          retrieve.device_video_id = DeviceVideo.id

          retrieve.metadata.correlation_stream_name = DeviceVideo.workflow

          retrieve
        end
      end
    end
  end
end
