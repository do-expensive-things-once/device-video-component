module DeviceVideoComponent
  module Controls
    module Workflow
      def self.example(increment: 1)
        "someStream-#{increment}"
      end
    end
  end
end

