module DeviceVideoComponent
  module Controls
    module Events
      module Requested
        def self.example
          requested = Messages::Events::Requested.build

          requested.device_id = DeviceVideo.device_id
          requested.start_minute = DeviceVideo.start_minute
          requested.device_video_id = DeviceVideo.id
          requested.workflow = DeviceVideo.workflow

          requested
        end
      end
    end
  end
end
