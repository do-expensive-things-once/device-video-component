module DeviceVideoComponent
  module Controls
    module Events 
      module Fetched
        def self.example
          fetched = Messages::Events::Fetched.build

          fetched.device_id = DeviceVideo.device_id
          fetched.start_minute = DeviceVideo.start_minute
          fetched.device_video_id = DeviceVideo.id
          fetched.uri = Uri.example

          fetched
        end
      end
    end
  end
end
