module DeviceVideoComponent
  module Controls
    module Events
      module Retrieved
        def self.example(workflow = nil)
          retrieved = Messages::Events::Retrieved.build

          retrieved.device_id = DeviceVideo.device_id
          retrieved.start_minute = DeviceVideo.start_minute
          retrieved.device_video_id = DeviceVideo.id
          retrieved.workflow = DeviceVideo.workflow
          retrieved.uri = Uri.example

          retrieved
        end
      end
    end
  end
end
