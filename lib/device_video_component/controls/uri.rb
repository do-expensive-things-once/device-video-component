module DeviceVideoComponent
  module Controls
    module Uri
      def self.example
        "video_data/part1.mp4 video_data/2022-10-22T00:00:00.mp4"
      end
    end
  end
end
