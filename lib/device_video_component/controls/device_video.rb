module DeviceVideoComponent
  module Controls
    module DeviceVideo
      def self.example
        New.example
      end

      def self.device_id
        ID.example(increment: id_increment)
      end

      def self.id_increment
        1111
      end

      def self.start_minute
        Time::StartMinute.example
      end

      def self.id
        "#{device_id}+#{start_minute}"
      end

      def self.workflow(increment: 1)
        Workflow.example(increment:)
      end

      def self.uri
        Uri.example
      end

      module New
        def self.example
          DeviceVideoComponent::DeviceVideo.build
        end
      end

      module Retrieved
        def self.example(workflow: nil)
          workflow ||= DeviceVideo.workflow

          device_video = New.example

          device_video.device_id = DeviceVideo.device_id
          device_video.start_minute = DeviceVideo.start_minute
          device_video.id = DeviceVideo.id
          device_video.uri = DeviceVideo.uri

          device_video.retrieved(workflow)

          device_video
        end
      end

      module Fetched
        def self.example
          device_video = New.example

          device_video.device_id = DeviceVideo.device_id
          device_video.start_minute = DeviceVideo.start_minute
          device_video.id = DeviceVideo.id
          device_video.uri = DeviceVideo.uri

          device_video
        end
      end
    end
  end
end
