module DeviceVideoComponent
  module Controls
    module DeviceVideo
      module Transform
        module RawData
          def self.example
            {
              id: DeviceVideo.id,
              device_id: DeviceVideo.device_id,
              start_minute: DeviceVideo.start_minute,
              uri: DeviceVideo.uri,
              requests: %Q{{ "#{DeviceVideo.workflow}": "retrieved" }}
            }
          end
        end
      end
    end
  end
end

