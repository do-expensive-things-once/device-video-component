module DeviceVideoComponent
  module Commands
    class Retrieve 
      include Dependency
      include Messaging::StreamName

      dependency :write, Messaging::Postgres::Write

      category :device_video

      def self.configure(receiver, attr_name: nil, session: nil)
        attr_name ||= :retrieve
        instance = build(session:)
        receiver.public_send("#{attr_name}=", instance)
      end

      def self.call(device_id:, start_minute:, previous_message: nil)
        instance = self.build
        instance.(device_id:, start_minute:, previous_message:)
      end

      def self.build(session: nil)
        instance = new
        instance.configure(session:)
        instance
      end

      def configure(session: nil)
        Messaging::Postgres::Write.configure(self, session:)
      end

      def call(device_id:, start_minute:, previous_message:)
        retrieve = Messages::Commands::Retrieve.build

        retrieve.metadata.follow(previous_message.metadata)

        device_video_id = "#{device_id}+#{start_minute}"

        retrieve.device_video_id = device_video_id
        retrieve.device_id = device_id
        retrieve.start_minute = start_minute

        stream_name = command_stream_name(device_video_id)

        write.(retrieve, stream_name)

        retrieve 
      end
    end
  end
end
