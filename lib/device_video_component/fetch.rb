module DeviceVideoComponent
  class Fetch
    def self.configure(receiver)
      instance = build

      receiver.fetch = instance
    end

    def self.build
      new
    end

    def call(device_id, start_minute)
      puts "started fetching the video, sleeping for 30 seconds"
      sleep 30
      puts "finished fetching the video, if it doesn't crash momentarily, you solved the problem!"

      File.join(Dir.pwd, 'video_data', "#{start_minute}.mp4")
    end

    module Substitute
      def self.build
        Fetch.new
      end

      class Fetch
        def call(device_id, start_minute)
          File.join(Dir.pwd, 'video_data', "#{start_minute}.mp4")
        end
      end
    end
  end
end
