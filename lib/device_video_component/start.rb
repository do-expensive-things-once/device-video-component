module DeviceVideoComponent
  module Start
    def self.call
      Consumers::Commands.start("deviceVideo:command")
    end
  end
end
