module DeviceVideoComponent
  class DeviceVideo
    include Schema::DataStructure

    attribute :id, String
    attribute :device_id, String
    attribute :start_minute, String
    attribute :uri, String
    attribute :requests, Hash, default: -> { {} }

    def retrieved(workflow)
      requests[workflow] = :retrieved
    end

    def retrieved?(workflow)
      requests[workflow] == :retrieved
    end

    def fetched?
      !!uri
    end

    def workflows
      requests.keys
    end

    module Transform
      def self.instance(raw_data)
        raw_data[:requests] = deserialize_requests(raw_data[:requests])

        DeviceVideo.build(raw_data)
      end

      def self.raw_data(instance)
        raw_data = instance.to_h

        raw_data[:requests] = instance.requests.to_json

        raw_data
      end

      def self.deserialize_requests(requests)
        parsed_requests = JSON.parse(requests)

        parsed_requests.transform_values(&:to_sym)
      end
    end
  end
end
