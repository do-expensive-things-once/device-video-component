module DeviceVideoComponent
  module Handlers
    class Commands
      include Messaging::Handle
      include Messaging::StreamName
      include Log::Dependency

      include Messages::Commands
      include Messages::Events

      dependency :write, Messaging::Postgres::Write
      dependency :store, Store
      dependency :fetch, Fetch

      def configure(session: nil)
        Messaging::Postgres::Write.configure(self, session:)
        Store.configure(self, session:)
        Fetch.configure(self)
      end

      category :device_video

      handle Retrieve do |retrieve|
        device_video_id = retrieve.device_video_id
        workflow = retrieve.metadata.correlation_stream_name
        start_minute = retrieve.start_minute

        device_video, version = store.fetch(device_video_id, include: :version)

        if device_video.retrieved?(workflow)
          logger.info(tag: :ignored) { "Video already retrieved for this workflow.  Event ignored (Event: #{requested.message_type}, DeviceVideo ID: #{device_video_id}, Workflow: #{workflow})" }
          return
        end

        events = []

        # This will be overwritten by the fetching if we haven't fetched
        uri = device_video.uri

        unless device_video.fetched?
          uri = fetch.(device_video.id, start_minute)

          fetched = Fetched.follow(retrieve, exclude: :workflow)

          fetched.uri = uri

          events << fetched
        end

        retrieved = Retrieved.follow(retrieve)
        retrieved.uri = uri
        retrieved.workflow = workflow 

        events << retrieved

        stream_name = stream_name(device_video_id)

        write.(events, stream_name, expected_version: version)
      end
    end
  end
end
