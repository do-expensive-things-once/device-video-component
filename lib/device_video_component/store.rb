module DeviceVideoComponent
  class Store
    include EntityStore

    category :device_video
    entity DeviceVideo
    projection Projection
    reader MessageStore::Postgres::Read, batch_size: 1000

    snapshot EntitySnapshot::Postgres, interval: 2
  end
end
