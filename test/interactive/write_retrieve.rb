require_relative './interactive_init'

retrieve = Controls::Commands::Retrieve.example
stream_name = "deviceVideo:command-#{retrieve.device_video_id}"

Messaging::Postgres::Write.(retrieve, stream_name)
