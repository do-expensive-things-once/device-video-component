require_relative './interactive_init'

# This test would catch the concurrency issue.
#
# Start the component before running this file.
#
# If you've solved the concurrency issue, you won't see a crash from a
# version mismatch error.

retrieve_1 = Controls::Commands::Retrieve.example
retrieve_1.metadata.correlation_stream_name = 'workflow1'

retrieve_2 = Controls::Commands::Retrieve.example
retrieve_2.metadata.correlation_stream_name = 'workflow2'

stream_name = "deviceVideo:command-#{retrieve_1.device_video_id}"

Messaging::Postgres::Write.(retrieve_1, stream_name)

sleep 10

Messaging::Postgres::Write.(retrieve_2, stream_name)
