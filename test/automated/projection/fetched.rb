require_relative "../automated_init"

context "Project" do
  context "Fetched" do
    fetched = Controls::Events::Fetched.example

    device_video_id = fetched.device_video_id or fail
    device_id = fetched.device_id or fail
    start_minute = fetched.start_minute or fail

    workflow = "someStream-123"

    device_video = Controls::DeviceVideo::New.example

    Projection.(device_video, fetched)

    test "fetched?" do
      assert(device_video.fetched?)
    end

    context "Attributes" do
      test "id" do
        assert(device_video.id == device_video_id)
      end

      test "device_id" do
        assert(device_video.device_id == device_id)
      end

      test "start_minute" do
        assert(device_video.start_minute == start_minute)
      end
    end
  end
end
