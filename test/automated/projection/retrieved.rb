require_relative "../automated_init"

context "Project" do
  context "Retrieved" do
    retrieved = Controls::Events::Retrieved.example

    device_video_id = retrieved.device_video_id or fail
    workflow = retrieved.workflow or fail

    device_video = Controls::DeviceVideo::Fetched.example

    Projection.(device_video, retrieved)

    test "retrieved?" do
      assert(device_video.retrieved?(workflow))
    end

    context "Attributes" do
      test "id" do
        assert(device_video.id == device_video_id)
      end
    end
  end
end
