require_relative '../../automated_init'

context "Commands" do
  context "Retrieve" do
    context "Previous Message" do
      previous_message = Controls::Message.example

      device_id = Controls::DeviceVideo.device_id
      start_minute = Controls::DeviceVideo.start_minute

      retrieve = Commands::Retrieve.new

      retrieve.(device_id:, start_minute:, previous_message:)

      write = retrieve.write

      retrieve_command = write.one_message do |written|
        written.instance_of?(Messages::Commands::Retrieve)
      end

      refute(retrieve_command.nil?)

      test "Retrieve command follows previous message" do
        assert(retrieve_command.follows?(previous_message))
      end
    end
  end
end
