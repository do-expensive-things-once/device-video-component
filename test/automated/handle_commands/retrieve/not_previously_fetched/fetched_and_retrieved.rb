require_relative "../../../automated_init"

context "Handle Commands" do
  context "Retrieve" do
    context "Not previously detched" do
      context "Fetched and Retrieved" do
        handler = Handlers::Commands.new

        retrieve = Controls::Commands::Retrieve.example

        device_video_id = retrieve.device_video_id or fail
        device_id = retrieve.device_id or fail
        start_minute = retrieve.start_minute or fail
        workflow = retrieve.metadata.correlation_stream_name or fail

        uri = File.join(Dir.pwd, 'video_data', "#{start_minute}.mp4")

        handler.(retrieve)

        writer = handler.write

        fetched = writer.one_message do |event|
          event.instance_of?(Messages::Events::Fetched)
        end

        retrieved = writer.one_message do |event|
          event.instance_of?(Messages::Events::Retrieved)
        end

        context "Fetched" do
          test "Wrote Fetched message" do
            refute(fetched.nil?)
          end

          test "Fetched is written to deviceVideo stream" do
            written_to_stream = writer.written?(fetched) do |stream_name|
              stream_name == "deviceVideo-#{device_id}+#{start_minute}"
            end

            assert(written_to_stream)
          end

          context "attributes" do
            test "device_video_id" do
              assert(fetched.device_video_id == device_video_id)
            end

            test "device_id" do
              assert(fetched.device_id == device_id)
            end

            test "start_minute" do
              assert(fetched.start_minute == start_minute)
            end

            test "uri" do
              assert(fetched.uri == uri)
            end
          end
        end

        context "Retrieved" do
          test "Wrote Retrieved message" do
            refute(retrieved.nil?)
          end

          test "Retrieved is written to deviceVideo stream" do
            written_to_stream = writer.written?(retrieved) do |stream_name|
              stream_name == "deviceVideo-#{device_id}+#{start_minute}"
            end

            assert(written_to_stream)
          end

          context "attributes" do
            test "device_video_id" do
              assert(retrieved.device_video_id == device_video_id)
            end

            test "device_id" do
              assert(retrieved.device_id == device_id)
            end

            test "start_minute" do
              assert(retrieved.start_minute == start_minute)
            end

            test "uri" do
              assert(retrieved.uri == uri)
            end
          end
        end
      end
    end
  end
end

