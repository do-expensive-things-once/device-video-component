require_relative "../../../automated_init"

context "Handle Commands" do
  context "Retrieve" do
    context "Not previously fetched" do
      context "Expected Version" do
        handler = Handlers::Commands.new

        retrieve = Controls::Commands::Retrieve.example

        device_video = Controls::DeviceVideo.example
        device_video.id = retrieve.device_video_id

        version = Controls::Version.example

        handler.store.add(retrieve.device_video_id, device_video, version)

        handler.(retrieve)

        writer = handler.write

        context "Fetched" do
          fetched = writer.one_message do |event|
            event.instance_of?(Messages::Events::Fetched)
          end

          test "Is entity version" do
            written_to_stream = writer.written?(fetched) do |_, expected_version|
              expected_version == version
            end

            assert(written_to_stream)
          end
        end

        context "Retrieved" do
          retrieved = writer.one_message do |event|
            event.instance_of?(Messages::Events::Retrieved)
          end

          test "Is entity version" do
            written_to_stream = writer.written?(retrieved) do |_, expected_version|
              expected_version == version
            end

            assert(written_to_stream)
          end
        end
      end
    end
  end
end
