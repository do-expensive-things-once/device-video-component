require_relative "../../../automated_init"

context "Handle Commands" do
  context "Retrieve" do
    context "Previously fetched" do
      context "Expected Version" do
        handler = Handlers::Commands.new

        retrieve = Controls::Commands::Retrieve.example
        workflow = retrieve.metadata.correlation_stream_name or fail

        device_video = Controls::DeviceVideo::Fetched.example
        device_video.fetched? or fail

        version = Controls::Version.example

        handler.store.add(retrieve.device_video_id, device_video, version)

        handler.(retrieve)

        writer = handler.write

        retrieved = writer.one_message do |event|
          event.instance_of?(Messages::Events::Retrieved)
        end

        test "Is entity version" do
          written_to_stream = writer.written?(retrieved) do |_, expected_version|
            expected_version == version
          end

          assert(written_to_stream)
        end
      end
    end
  end
end
