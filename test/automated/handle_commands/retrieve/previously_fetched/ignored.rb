require_relative "../../../automated_init"

context "Handle Commands" do
  context "Retrieve" do
    context "Previously fetched" do
      context "Ignored" do
        handler = Handlers::Commands.new

        retrieve = Controls::Commands::Retrieve.example

        device_video_id = retrieve.device_video_id or fail
        workflow = retrieve.metadata.correlation_stream_name or fail

        device_video = Controls::DeviceVideo::Retrieved.example(workflow:)
        device_video.fetched? or fail

        handler.store.add(device_video_id, device_video)

        handler.(retrieve)

        writer = handler.write

        retrieved = writer.one_message do |event|
          event.instance_of?(Messages::Events::Retrieved)
        end

        test "No message written" do
          assert(retrieved.nil?)
        end
      end
    end
  end
end

