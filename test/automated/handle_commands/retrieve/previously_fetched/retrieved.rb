require_relative "../../../automated_init"

context "Handle Commands" do
  context "Retrieve" do
    context "Previously fetched" do
      context "Retrieved" do
        handler = Handlers::Commands.new

        retrieve = Controls::Commands::Retrieve.example

        device_video_id = retrieve.device_video_id or fail
        device_id = retrieve.device_id or fail
        start_minute = retrieve.start_minute or fail
        workflow = retrieve.metadata.correlation_stream_name or fail

        device_video = Controls::DeviceVideo::Fetched.example
        uri = device_video.uri or fail
        refute(device_video.retrieved?(workflow))

        handler.store.add(device_video_id, device_video)

        handler.(retrieve)

        writer = handler.write

        retrieved = writer.one_message do |event|
          event.instance_of?(Messages::Events::Retrieved)
        end

        test "Wrote Retrieved message" do
          refute(retrieved.nil?)
        end

        test "Written to deviceVideo stream" do
          written_to_stream = writer.written?(retrieved) do |stream_name|
            stream_name == "deviceVideo-#{device_id}+#{start_minute}"
          end

          assert(written_to_stream)
        end

        context "attributes" do
          test "device_video_id" do
            assert(retrieved.device_video_id == device_video_id)
          end

          test "device_id" do
            assert(retrieved.device_id == device_id)
          end

          test "start_minute" do
            assert(retrieved.start_minute == start_minute)
          end

          test "workflow" do
            assert(retrieved.metadata.correlation_stream_name == workflow)
            assert(retrieved.workflow == workflow)
          end

          test "uri" do
            assert(retrieved.uri == uri)
          end
        end
      end
    end
  end
end

