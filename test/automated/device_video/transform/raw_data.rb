require "json"

require_relative "../../automated_init"

context "DeviceVideo" do
  context "Transform" do
    context "raw_data" do
      device_video = Controls::DeviceVideo::Retrieved.example

      device_video_id = device_video.id or fail
      device_id = device_video.device_id or fail
      start_minute = device_video.start_minute or fail
      uri = device_video.uri or fail

      raw_data = DeviceVideo::Transform.raw_data(device_video)

      test "id" do
        assert(raw_data[:id] == device_video_id)
      end

      test "device_id" do
        assert(raw_data[:device_id] == device_id)
      end

      test "start_minute" do
        assert(raw_data[:start_minute] == start_minute)
      end

      test "uri" do
        assert(raw_data[:uri] == uri)
      end

      test "requests" do
        deserialized_requests = DeviceVideo::Transform.deserialize_requests(raw_data[:requests])
        assert(deserialized_requests == device_video.requests)
      end
    end
  end
end
