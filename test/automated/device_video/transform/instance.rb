require_relative "../../automated_init"

context "DeviceVideo" do
  context "Transform" do
    context "instance" do
      raw_data = Controls::DeviceVideo::Transform::RawData.example

      requests = DeviceVideo::Transform.deserialize_requests(raw_data[:requests])

      instance = DeviceVideo::Transform.instance(raw_data)

      test "requests" do
        assert(instance.requests == requests)
      end
    end
  end
end
