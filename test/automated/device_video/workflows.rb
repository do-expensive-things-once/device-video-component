require_relative "../automated_init"

context "DeviceVideo" do
  context "workflows" do
    device_video = Controls::DeviceVideo::New.example

    device_video.workflows.empty? or fail

    workflow = Controls::DeviceVideo.workflow

    device_video.retrieved(workflow)

    test "workflows contains workflow" do
      assert(device_video.workflows == [workflow])
    end
  end
end
