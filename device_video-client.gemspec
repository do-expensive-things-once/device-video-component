# -*- encoding: utf-8 -*-
Gem::Specification.new do |s|
  s.name = 'device_video-client'
  s.version = '0.0.0'
  s.summary = 'Device Video Client'
  s.description = ' '

  s.authors = ['Ethan Garofolo']
  s.email = 'ethan@suchsoftware.com'
  s.homepage = 'https://gitlab.com/such-software/do-expensive-things-once/device-video-component'
  s.licenses = ['MIT']

  s.require_paths = ['lib']
  s.files = Dir.glob('{lib}/**/*')
  s.platform = Gem::Platform::RUBY
  s.required_ruby_version = '>= 2.4'

  s.require_paths = ['lib']

  files = Dir["lib/device_video/**/*.rb"]

  files += Dir["lib/device_video_component/{controls.rb,controls/**/*.rb}"]

  files << "lib/device_video_component/load.rb"

  File.read("lib/device_video_component/load.rb").each_line.map do |line|
    next if line == "\n"

    _, filename = line.split(/[[:blank:]]+/, 2)

    filename.gsub!(/['"]/, '')
    filename.strip!

    filename = File.join('lib', "#{filename}.rb")

    if File.exist?(filename)
      files << filename
    end
  end

  s.files = files

  s.platform = Gem::Platform::RUBY
  s.required_ruby_version = '>= 2.4'

  s.add_runtime_dependency 'evt-messaging-postgres'

  s.add_development_dependency 'test_bench'
end
