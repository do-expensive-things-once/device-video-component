# device_video_component

This component "retrieves" "videos" from "devices".

It simulates the high-latency of a cellular-connected device

# Running the project

Install dependencies: `POSTURE=development ./install-gems.sh`

Start Message DB: `docker-compose up`

Start the component: `script/start`

# Tests

`./test.sh`

# Copyright

Copyright 2022 Such Software, LLC.

# video_data

I don't own the copyright on that.  I think this falls under fair use.

It's not the whole song, and it's for academic purposes.

# License

MIT
